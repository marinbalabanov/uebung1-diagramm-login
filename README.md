# Login-Prozess

Das ist ein Programmablaufplan für die Planung des Login-Prozesses auf einer Website, wie ein Webinterface funktionieren könnte mit Login und anderen Funktionen.

Hier wird die Kommunikation beschrieben und sichergestellt, dass geschützte Inhalte nicht direkt aufrufbar sind.

![Diagramm des Login-Prozesses](assets/login-prozess-balabanov.png)